import { render } from 'solid-js/web';
import App from './components/App';
import './reset.css';
import './index.css';

render(() => <App />, document.getElementById('root'));
