/* eslint-disable jest/no-conditional-expect */
import * as CreditCardService from '../creditCardService';

describe('CreditCardService', () => {
  it('Should Reject The Date', async () => {
    const expirationDate = new Date();
    expirationDate.setMonth(expirationDate.getFullYear() - 1);

    try {
      await CreditCardService.validateCreditCard({
        cardHolder: 'John Doe',
        cardNumber: '4321123487655678',
        expirationDate,
        crypto: '567'
      });
    } catch (err) {
      expect(err.message).toEqual('Expiration Date Exceeded');
    }
  });

  it('Should Reject The Number', async () => {
    const expirationDate = new Date();
    expirationDate.setMonth(expirationDate.getFullYear() + 1);

    try {
      await CreditCardService.validateCreditCard({
        cardHolder: 'John Doe',
        cardNumber: '1234432156788765',
        expirationDate,
        crypto: '567'
      });
    } catch (err) {
      expect(err.message).toEqual('Unsupported Card Number');
    }
  });

  it('Should Return A Message', async () => {
    const expirationDate = new Date();
    expirationDate.setMonth(expirationDate.getFullYear() + 1);

    const res = await CreditCardService.validateCreditCard({
      cardHolder: 'John Doe',
      cardNumber: '4321123487655678',
      expirationDate,
      crypto: '567'
    });

    expect(res.message).toEqual('Credit Card Saved');
  });
});
