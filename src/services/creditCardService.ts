import { CREDIT_CARD_REJECTED, CREDIT_CARD_RESOLVED, EXPIRATION_CARD_REJECTED } from '../constants';

interface CreditCardParams {
  cardHolder: string;
  cardNumber: string;
  expirationDate: Date;
  crypto: string;
}

/**
 * @method validateCreditCard
 * @param {CreditCardParams} params Card Data Params
 * @returns {Promise}
 */
export const validateCreditCard = ({
  cardHolder,
  cardNumber,
  expirationDate,
  crypto
}: CreditCardParams): Promise<{ message: string }> => {
  const now = new Date();

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const first = cardNumber.substring(0, 1);

      if (cardNumber.length < 16 || (+first !== 4 && +first !== 5)) {
        const error = new Error(CREDIT_CARD_REJECTED);
        return reject(error);
      }

      if (now.getTime() > expirationDate.getTime()) {
        const error = new Error(EXPIRATION_CARD_REJECTED);
        return reject(error);
      }

      return resolve({ message: CREDIT_CARD_RESOLVED });
    }, 1 * 1000);
  });
};
