import { render } from 'solid-testing-library';
import Recto from '../Recto';

describe('<Recto />', () => {
  it('Should Match The Snapshot', () => {
    const { container, unmount } = render(() => (
      <Recto cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34">
        <span>Hello World</span>
      </Recto>
    ));

    expect(container).toMatchSnapshot();
    unmount();
  });

  it('Should Renders', () => {
    const { getByText, unmount } = render(() => (
      <Recto cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34">
        <span>Hello World</span>
      </Recto>
    ));

    expect(getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(getByText('JOHN DOE')).toBeInTheDocument();
    expect(getByText('12/34')).toBeInTheDocument();
    expect(getByText('Hello World')).toBeInTheDocument();
    unmount();
  });
});
