import { render } from 'solid-testing-library';
import Verso from '../Verso';

describe('<Verso />', () => {
  it('Should Match The Snapshot', () => {
    const { container, unmount } = render(() => (
      <Verso cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />
    ));

    expect(container).toMatchSnapshot();
    unmount();
  });

  it('Should Renders', () => {
    const { getByText, unmount } = render(() => (
      <Verso cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />
    ));

    expect(getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(getByText('JOHN DOE')).toBeInTheDocument();
    expect(getByText('12/34')).toBeInTheDocument();
    expect(getByText('567')).toBeInTheDocument();
    unmount();
  });
});
