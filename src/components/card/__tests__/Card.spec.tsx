import { render } from 'solid-testing-library';
import Card from '../Card';

describe('<Card />', () => {
  it('Should Match The Snapshot', () => {
    const { container, unmount } = render(() => (
      <Card reversed cardNumber="4321 1234 8765 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />
    ));

    expect(container).toMatchSnapshot();
    unmount();
  });

  it('Should Renders', () => {
    const { getAllByText, getByText, unmount } = render(() => (
      <Card reversed cardNumber="4321 1234 8765 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />
    ));

    expect(getAllByText('**** 1234 **** 5678')).toHaveLength(2);
    expect(getAllByText('JOHN DOE')).toHaveLength(2);
    expect(getAllByText('12/34')).toHaveLength(2);
    expect(getByText('567')).toBeInTheDocument();
    unmount();
  });
});
