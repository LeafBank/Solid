import { Chip, NearFieldCom } from '../icons';
import './recto.css';

type Props = {
  cardNumber: string;
  fullName: string;
  month: string;
  year: string;
  children: JSX.Element;
};

function Recto(props: Props) {
  return (
    <div class="recto">
      <div class="half">
        <div style={{ display: 'flex', padding: '0.5rem' }}>
          <div class="big-leaf">
            <div class="medium-leaf">
              <div class="small-leaf" />
            </div>
          </div>

          <p class="leaf-bank col">
            <span class="title">LEAF</span>
            <span class="subtitle">bank</span>
          </p>

          {props.children}
        </div>

        <div class="icons">
          <Chip style={{ 'margin-left': '0.5rem' }} />
          <NearFieldCom style={{ 'margin-left': '0.5rem' }} />
        </div>
      </div>

      <div class="half">
        <p class="card-number">{props.cardNumber}</p>
        <div class="corners">
          <p class="card-corner">
            <span class="key">fullname</span>
            <span class="value truncate">{props.fullName || 'Unknown'}</span>
          </p>

          <p class="card-corner">
            <span class="key">expires</span>
            <span class="value">
              {props.month}/{props.year}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Recto;
