import './verso.css';

type Props = {
  cardNumber: string;
  fullName: string;
  month: string;
  year: string;
  crypto: string;
};

function Verso(props: Props) {
  return (
    <div class="verso">
      <div class="half">
        <p class="leaf-bank row">
          <span class="title">LEAF</span>
          <span class="subtitle">bank</span>
        </p>
        <div class="magnetic-tape" />
      </div>

      <div class="half">
        <p class="card-number">{props.cardNumber}</p>
        <p class="crypto">{props.crypto}</p>

        <div class="corners">
          <p class="card-corner">
            <span class="value truncate">{props.fullName || 'Unknown'}</span>
          </p>
          <p class="card-corner">
            <span class="value">
              {props.month}/{props.year}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Verso;
