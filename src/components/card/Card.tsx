import { MasterCard, Visa } from '../icons';
import Recto from './Recto';
import Verso from './Verso';
import { PATTERN } from '../../constants';

type Props = {
  reversed: boolean;
  cardNumber: string;
  fullName: string;
  month: string;
  year: string;
  crypto: string;
};

function Card(props: Props) {
  const cardNumber = () => {
    return PATTERN.replace(/\*/g, (char, idx) => {
      if ((idx >= 5 && idx <= 8) || (idx >= 15 && idx <= 18)) {
        return props.cardNumber[idx] || char;
      }
      return char;
    });
  };

  return (
    <div
      class="card"
      style={{
        transform: props.reversed ? 'rotateY(180deg)' : 'rotateY(0deg)'
      }}>
      <Recto cardNumber={cardNumber()} fullName={props.fullName} month={props.month} year={props.year}>
        {props.cardNumber.startsWith('4') && <Visa />}
        {props.cardNumber.startsWith('5') && <MasterCard />}
      </Recto>

      <Verso
        cardNumber={cardNumber()}
        fullName={props.fullName}
        month={props.month}
        year={props.year}
        crypto={props.crypto}
      />
    </div>
  );
}

export default Card;
