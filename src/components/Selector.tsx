import { createSignal, mergeProps, Show } from 'solid-js';

type Props = {
  defaultValue?: string;
  onClick?: (e: Event<HTMLButtonElement>) => void;
  ariaLabelledBy?: string;
  children: ({ onClose }: { onClose: (value: string) => void }) => JSX.Element;
};

function Selector(props: Props) {
  const merged = mergeProps({ defaultValue: 'Unknown', onClick: () => {} }, props);
  const [value, setValue] = createSignal('');
  const [displayList, setDisplayList] = createSignal(false);

  const onClose = (value: string) => {
    setValue(value);
    setDisplayList(false);
  };

  return (
    <div style={{ position: 'relative' }}>
      <button
        class="selector"
        type="button"
        onClick={e => {
          props.onClick(e);
          setDisplayList(displayList => !displayList);
        }}
        aria-haspopup="listbox"
        aria-expanded={displayList()}
        aria-labelledby={props.ariaLabelledBy}>
        {value() || props.defaultValue}
        <svg width={16} height={16} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M8 9l4-4 4 4m0 6l-4 4-4-4"
            stroke="currentColor"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </button>

      <Show when={displayList()}>
        <ul class="listbox" tabIndex={-1} role="listbox">
          {props.children({ onClose })}
        </ul>
      </Show>
    </div>
  );
}

export default Selector;
