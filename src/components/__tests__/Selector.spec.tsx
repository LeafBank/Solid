import { render } from 'solid-testing-library';
import userEvent from '@testing-library/user-event';
import Selector from '../Selector';

describe('<Selector />', () => {
  it('Should Match The Snapshot', () => {
    const { container, unmount } = render(() => <Selector>{() => <span>Hello World</span>}</Selector>);

    expect(container).toMatchSnapshot();
    unmount();
  });

  it('Should Renders', () => {
    const { queryByText, unmount } = render(() => (
      <Selector defaultValue="Hello World" ariaLabelledBy="helloWorld">
        {() => <span>Lorem Ipsum</span>}
      </Selector>
    ));

    expect(queryByText('Lorem Ipsum')).not.toBeInTheDocument();
    unmount();
  });

  it('Should Emit Events', () => {
    const onClickMock = vi.fn();

    const { getByText, queryByText, unmount } = render(() => (
      <Selector defaultValue="Hello World" onClick={onClickMock} ariaLabelledBy="helloWorld">
        {() => <span>Lorem Ipsum</span>}
      </Selector>
    ));

    const buttonElement = getByText('Hello World');

    userEvent.click(buttonElement);

    expect(queryByText('Lorem Ipsum')).toBeInTheDocument();
    expect(onClickMock).toHaveBeenCalled();
    unmount();
  });
});
