import { render, fireEvent } from 'solid-testing-library';
import Field from '../Field';

describe('<Field />', () => {
  it('Should Match The Snapshot', () => {
    const { container, unmount } = render(() => <Field id="field" />);

    expect(container).toMatchSnapshot();
    unmount();
  });

  it('Should Renders', () => {
    const { queryByText, queryByPlaceholderText, unmount } = render(() => (
      <Field
        classNames="row"
        style={{ margin: 'auto' }}
        id="field"
        name="field"
        label="Field"
        type="text"
        placeholder="Field"
        maxLength={10}
      />
    ));

    expect(queryByText('Field')).toBeInTheDocument();
    expect(queryByPlaceholderText('Field')).toBeInTheDocument();
    unmount();
  });

  it('Should Emit Events', () => {
    const onInputMock = vi.fn();
    const onFocusMock = vi.fn();

    const { getByPlaceholderText, queryByPlaceholderText, unmount } = render(() => (
      <Field
        classNames="row"
        style={{ margin: 'auto' }}
        id="field"
        name="field"
        label="Field"
        type="text"
        placeholder="Field"
        maxLength={10}
        onInput={onInputMock}
        onFocus={onFocusMock}
      />
    ));

    const fieldElement = getByPlaceholderText('Field');

    fireEvent.focus(fieldElement);
    fireEvent.change(fieldElement, { target: { value: 'Hello World' } });

    expect(queryByPlaceholderText('Field')).toHaveValue('Hello World');
    expect(onInputMock).toHaveBeenCalled();
    expect(onFocusMock).toHaveBeenCalled();
    unmount();
  });
});
