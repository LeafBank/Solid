import { render } from 'solid-testing-library';
import Modal from '../Modal';

describe('<Modal />', () => {
  it('Should Match The Snapshot', () => {
    const { container, unmount } = render(() => <Modal isVisible title="Hello World" />);

    expect(container).toMatchSnapshot();
    unmount();
  });

  it('Should Renders As Resolved', () => {
    const { container, getByText, unmount } = render(() => <Modal isVisible title="Hello World" />);

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(16, 185, 129);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
    unmount();
  });

  it('Should Renders As Rejected', () => {
    const { container, getByText, unmount } = render(() => <Modal isVisible isError title="Hello World" />);

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(239, 68, 68);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
    unmount();
  });
});
