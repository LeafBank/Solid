import { createEffect, createSignal, For, Signal } from 'solid-js';
import { Card } from './card';
import Field from './Field';
import Modal from './Modal';
import Selector from './Selector';
import * as CreditCardService from '../services/creditCardService';
import { getYears, convertMonthToInt, convertYearToInt } from '../utils';
import { MONTHS } from '../constants';

function createField(initialState: string): [string, (e: Event<HTMLInputElement>) => void] {
  const [value, setValue] = createSignal(initialState);
  return [value, e => setValue(e.target.value)];
}

interface CreateModal {
  title: Signal<string>;
  setTitle: (value: string) => void;
  isVisible: Signal<boolean>;
  visibilityOn: () => void;
  visibilityOff: () => void;
  error: Signal<boolean>;
  setError: (value: boolean) => void;
}

function createModal(): CreateModal {
  const [title, setTitle] = createSignal('');
  const [visibility, setVisibility] = createSignal(false);
  const [error, setError] = createSignal(false);

  return {
    title,
    setTitle,
    isVisible: visibility,
    visibilityOn: () => setVisibility(true),
    visibilityOff: () => setVisibility(false),
    error,
    setError
  };
}

function App() {
  const [reversed, setReversed] = createSignal(false);
  const [cardHolder, setCardHolder] = createField('');
  const [cardNumber, setCardNumber] = createField('');
  const [month, setMonth] = createSignal('');
  const [year, setYear] = createSignal('');
  const [crypto, setCrypto] = createField('');
  const [loading, setLoading] = createSignal(false);
  const modal = createModal();

  createEffect(() => {
    // eslint-disable-next-line
    console.log('Leaf Bank');
  });

  // TODO: Move To Utils
  const isValid = (): boolean => {
    if (cardHolder().length === 0) {
      return false;
    }

    if (cardNumber().length === 0) {
      return false;
    }

    if (month().length === 0) {
      return false;
    }

    if (year().length === 0) {
      return false;
    }

    if (crypto().length === 0) {
      return false;
    }

    return true;
  };

  const handleSubmit = (e: Event<HTMLFormElement>) => {
    e.preventDefault();

    setLoading(true);

    const expirationDate = new Date();
    expirationDate.setMonth(+month());
    expirationDate.setFullYear(+year());

    CreditCardService.validateCreditCard({
      cardHolder: cardHolder(),
      cardNumber: cardNumber().replace(/\s/g, ''), // NOTE: Matches Whitespaces
      expirationDate,
      crypto: crypto()
    })
      .then(res => {
        modal.setTitle(res.message);

        setTimeout(() => {
          modal.visibilityOn();
        }, 0.5 * 1000);
      })
      .catch(err => {
        modal.setError(true);
        modal.setTitle(err.message);

        setTimeout(() => {
          modal.visibilityOn();
        }, 0.5 * 1000);
      })
      .finally(() => setLoading(false));
  };

  return (
    <div>
      <Show when={modal.title()}>
        <Modal isVisible={modal.isVisible()} title={modal.title()} isError={modal.error()} />
      </Show>

      <Card
        reversed={reversed()}
        cardNumber={cardNumber()}
        fullName={cardHolder()}
        month={convertMonthToInt(month())}
        year={convertYearToInt(year())}
        crypto={crypto()}
      />

      <form onSubmit={handleSubmit}>
        <Field
          classNames="row"
          style={{ 'margin-top': 'auto' }}
          id="cardHolder"
          label="Card Holder"
          placeholder="John Doe"
          onInput={setCardHolder}
          onFocus={() => setReversed(false)}
        />

        <Field
          classNames="row"
          id="cardNumber"
          label="Card Number"
          placeholder="5678 1234 5678 1234"
          maxLength={19}
          onInput={e => {
            e.target.value = e.target.value
              .replace(/[^\dA-Z]/g, '') // NOTE: Escapes Digits, Matches Words
              .replace(/(.{4})/g, '$1 ') // NOTE: Any Chars, 4 Times
              .trim();
            setCardNumber(e);
          }}
          onFocus={() => setReversed(false)}
        />

        <div className="field-group">
          <div className="field" style={{ width: '100px' }}>
            <label htmlFor="expiresThru">Expires Thru</label>
            <Selector defaultValue="Month" onClick={() => setReversed(false)} ariaLabelledBy="expiresThru">
              {({ onClose }) => (
                <For each={MONTHS}>
                  {(val, idx) => (
                    <li
                      key={idx}
                      id={`listbox-option-${idx}`}
                      className={`option${val === month ? ' selected' : ''}`}
                      role="option"
                      aria-selected={val === month}
                      onClick={() => {
                        setMonth(val);
                        setReversed(false);
                        onClose(val);
                      }}>
                      {val}
                    </li>
                  )}
                </For>
              )}
            </Selector>
          </div>

          <div className="field" style={{ width: '100px' }}>
            <Selector defaultValue="Year" onClick={() => setReversed(false)}>
              {({ onClose }) => (
                <For each={getYears(5, 20)}>
                  {(val, idx) => (
                    <li
                      key={idx}
                      id={`listbox-option-${idx}`}
                      className={`option${val === year ? ' selected' : ''}`}
                      role="option"
                      aria-selected={val === year}
                      onClick={() => {
                        setYear(val);
                        setReversed(false);
                        onClose(val);
                      }}>
                      {val}
                    </li>
                  )}
                </For>
              )}
            </Selector>
          </div>

          <Field
            style={{ width: '50px' }}
            id="crypto"
            label="CVV"
            placeholder="567"
            maxLength={3}
            onInput={e => {
              e.target.value = e.target.value
                .replace(/[^\dA-Z]/g, '') // NOTE: Escapes Digits, Matches Words
                .trim();
              setCrypto(e);
            }}
            onFocus={() => setReversed(true)}
          />
        </div>

        <button class="submit" disabled={loading() || !isValid()}>
          {loading() ? 'Loading' : 'Submit'}
        </button>
      </form>
    </div>
  );
}

export default App;
