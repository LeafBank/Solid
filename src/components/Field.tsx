import { mergeProps, Show } from 'solid-js';

type Props = {
  classNames?: string;
  id: string;
  name?: string;
  label?: string;
  type?: string;
  placeholder?: string;
  maxLength?: number;
  onInput?: (e: Event<HTMLInputElement>) => void;
  onFocus?: (e: Event<HTMLInputElement>) => void;
};

function Field(props: Props) {
  const merged = mergeProps({ style: {}, type: 'text', onInput: () => {}, onFocus: () => {} }, props);

  return (
    <div class={`field ${props.classNames}`} style={merged.style}>
      <Show when={props.label}>
        <label htmlFor={props.id}>{props.label}</label>
      </Show>

      <input
        id={props.id}
        name={props.name || props.id}
        type={merged.type}
        placeholder={props.placeholder}
        maxLength={props.maxLength}
        onInput={merged.onInput}
        onFocus={merged.onFocus}
      />
    </div>
  );
}

export default Field;
