import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';

export default defineConfig({
  plugins: [solidPlugin()],
  test: {
    environment: 'jsdom',
    globals: true,
    transformMode: {
      web: [/\.[jt]sx?$/]
    },
    setupFiles: './src/setupTests.ts',
    deps: {
      inline: [/solid-js/]
    },
    threads: false,
    isolate: false
  },
  build: {
    target: 'esnext'
  },
  resolve: {
    conditions: ['development', 'browser']
  },
  server: {
    port: 3000
  }
});
