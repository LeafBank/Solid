# Leaf Bank 🍃

> 1.0.1

Application made with 💙 and [Solid](https://www.solidjs.com/)

Hosted by [Netlify](https://leafbank-solid.netlify.app/)

## Process

Repository:

```
git clone https://gitlab.com/LeafBank/Solid.git
```

Install:

```
npm install
```

Dev:

```
npm run start
```

Testing:

```
npm run test
```

Build:

```
npm run build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
